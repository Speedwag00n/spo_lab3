#include "files_manager.h"
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <openssl/md5.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include "../utils/consts.c"


int get_file_size(char *file_path)
{
    FILE *file = fopen(file_path, "rb");

    fseek(file, 0, SEEK_END);
    int result = ftell(file);

    fclose(file);

    return result;
}

char *get_file_hash(char *file_path)
{
    unsigned char buffer[READ_FILE_BUFFER_SIZE];
    unsigned char *hash = calloc(1, MD5_DIGEST_LENGTH);

    MD5_CTX context;
    MD5_Init(&context);

    int file_descriptor = open(file_path, O_RDONLY);

    char continue_read = 1;

    while (continue_read)
    {
        int bytes_read = read(file_descriptor, buffer, READ_FILE_BUFFER_SIZE);

        if (bytes_read <= 0)
        {
            continue_read = 0;
        }
        else
        {
            MD5_Update(&context, buffer, (unsigned long) continue_read);
        }
    }

    close(file_descriptor);

    MD5_Final(hash, &context);

    return hash;
}

void process_dir(char *root_path, char *dir_path, struct files_list_wrapper *files_list_wrapper)
{
    char *current_dir_path = calloc(1, MAX_PATH_LENGTH);
    strcat(current_dir_path, root_path);
    strcat(current_dir_path, "/");
    strcat(current_dir_path, dir_path);

    DIR *dir = opendir(current_dir_path);

    if (dir)
    {
        struct dirent *file;

        while ((file = readdir(dir)) != NULL)
        {
            // skip "."" and ".." files
            if (!strcmp(file->d_name, ".") || !strcmp(file->d_name, ".."))
            {
                continue;
            }
            else if (file->d_type == 4)
            {
                char *next_dir_path = calloc(1, MAX_PATH_LENGTH);

                if (strcmp("", dir_path)) {
                    strcat(next_dir_path, dir_path);
                    strcat(next_dir_path, "/");
                }
                strcat(next_dir_path, file->d_name);

                process_dir(root_path, next_dir_path, files_list_wrapper);

                free(next_dir_path);
            }
            else if (file->d_type == 8)
            {
                struct file *file_struct = calloc(1, sizeof(struct file));

                char *file_name = calloc(1, MAX_FILE_NAME_LENGTH);
                strcat(file_name, file->d_name);

                char *file_path = calloc(1, MAX_PATH_LENGTH);
                strcat(file_path, current_dir_path);
                strcat(file_path, "/");
                strcat(file_path, file->d_name);

                // get file name, size and hash
                file_struct->name = file_name;

                file_struct->size = get_file_size(file_path);

                file_struct->md5_hash = get_file_hash(file_path);

                files_list_wrapper->files_list = g_list_append(files_list_wrapper->files_list, file_struct);

                char *md5_buffer = calloc(1, MD5_DIGEST_LENGTH);
                char *md5_hash = calloc(1, MD5_DIGEST_LENGTH * 2);

                for(int i = 0; i < MD5_DIGEST_LENGTH; i++)
                {
                    sprintf(md5_buffer, "%02x", (unsigned char)file_struct->md5_hash[i]);
                    strcat(md5_hash, md5_buffer);
                }

                char *relative_file_path = calloc(1, MAX_PATH_LENGTH);
                if (strcmp("", dir_path)) {
                    strcat(relative_file_path, dir_path);
                    strcat(relative_file_path, "/");
                }
                strcat(relative_file_path, file->d_name);

                file_struct->relative_path = relative_file_path;

                free(file_struct->md5_hash);
                free(md5_buffer);
                file_struct->md5_hash = md5_hash;
            }
        }

        closedir(dir);
    }

    free(current_dir_path);
}

int get_shared_files(char *working_dir_path, struct files_list_wrapper *files_list_wrapper)
{
    DIR *working_dir = opendir(working_dir_path);

    if (working_dir)
    {
        process_dir(working_dir_path, "", files_list_wrapper);

        closedir(working_dir);
    }
    else
    {
        return 1;
    }

    return 0;
}


void free_file(struct file *file)
{
    free(file->name);
    free(file->md5_hash);
    free(file->relative_path);
}


struct file *description_to_file(char *file_description)
{
    struct file *file = calloc(1, sizeof(struct file));

    char *file_description_copy = file_description;
    int filename_found = 0;
    int size_found = 0;
    int hash_found = 0;
    char *first_char = file_description_copy;

    while (!filename_found)
    {
        if (*(file_description_copy++) == '/')
        {
            filename_found = 1;
        }
    }

    file->name = calloc(1, MAX_FILE_NAME_LENGTH);
    strncpy(file->name, first_char, file_description_copy - first_char - 1);
    file->relative_path = calloc(1, MAX_FILE_NAME_LENGTH);
    strncpy(file->relative_path, first_char, file_description_copy - first_char - 1);

    first_char = file_description_copy;

    while (!size_found)
    {
        if (*(file_description_copy++) == '/')
        {
            size_found = 1;
        }
    }

    char *size_buffer = calloc(1, MAX_FILE_NAME_LENGTH);
    strncpy(size_buffer, first_char, file_description_copy - first_char - 1);
    file->size = atoi(size_buffer);
    free(size_buffer);

    first_char = file_description_copy;

    while (!hash_found)
    {
        if (*(file_description_copy++) == '\0')
        {
            hash_found = 1;
        }
    }

    file->md5_hash = calloc(1, MD5_DIGEST_LENGTH * 2);
    strncpy(file->md5_hash, first_char, file_description_copy - first_char - 1);

    return file;
}
