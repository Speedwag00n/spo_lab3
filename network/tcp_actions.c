#include "tcp_actions.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include "../ui/ui.h"
#include "../utils/consts.c"
#include "../command_line/command_interpreter.h"
#include "../files/files_manager.h"


void take_tcp_port(struct global_structure *global_structure, struct tcp_port_description *tcp_port_description)
{
    int socket_fd;

    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        ui_print(global_structure->windows_state, "Failed to create socket (listener TCP socket)", 1);
        tcp_port_description->port = -1;
        return;
    }

    struct sockaddr_in address;
    memset(&address, 0, sizeof(address));

    int port = DEFAULT_TCP_PORT;

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    while (bind(socket_fd, (const struct sockaddr *) &address, sizeof(address)) < 0)
    {
        address.sin_port = htons(++port);
    }

    tcp_port_description->port = port;
    tcp_port_description->socket_fd = socket_fd;
    tcp_port_description->address = address;
}


void create_tcp_listener(struct tcp_listener_thread_struct *tcp_listener_thread_struct)
{
	struct global_structure *global_structure = tcp_listener_thread_struct->global_structure;
    struct tcp_port_description *tcp_port_description = tcp_listener_thread_struct->tcp_port_description;

    int address_length = sizeof(tcp_port_description->address);

    if ((listen(tcp_port_description->socket_fd, 3)) < 0)
    {
        ui_print(global_structure->windows_state, "Could not start listen (listener TCP socket)", 1);
        close(tcp_port_description->socket_fd);
        free(tcp_port_description);
        free(tcp_listener_thread_struct);
        return;
    }

    int new_socket;

    if ((new_socket = accept(tcp_port_description->socket_fd, 
    	(struct sockaddr *)&tcp_port_description->address, (socklen_t*)&address_length))<0) 
    {
        ui_print(global_structure->windows_state, "Could not accept connection (listener TCP socket)", 1);
        close(tcp_port_description->socket_fd);
        free(tcp_port_description);
        free(tcp_listener_thread_struct);
        return;
    }

    char buffer[MAX_ARG_LENGTH];
    sprintf(buffer, "Start TCP listener on port %d", tcp_port_description->port);
    ui_print(global_structure->windows_state, buffer, 1);

    char *file_path_buffer = calloc(1, MAX_PATH_LENGTH);
    strcat(file_path_buffer, global_structure->root_path);
    strcat(file_path_buffer, "/");
    strcat(file_path_buffer, tcp_listener_thread_struct->file->relative_path);

    int file_descriptor = open(file_path_buffer, O_RDONLY);

    if (file_descriptor >= 0)
    {
        char uploading_finished = 0;

        char *message_buffer = calloc(1, MESSAGE_BUFFER);

        while (!uploading_finished)
        {
	    	read(new_socket, message_buffer, MESSAGE_BUFFER);

	        if(message_buffer[0] == ' ')
	        {
			    struct uploading_file *uploading_file = calloc(1, sizeof(struct uploading_file));
			    uploading_file->file_description = tcp_listener_thread_struct->file_description;
			    uploading_file->message = calloc(1, MAX_LOG_ROW_LENGTH);
			    strcat(uploading_file->message, message_buffer);

                update_uploading_file(global_structure->windows_state, uploading_file);
	        }
	        else if (strcmp("close", message_buffer))
	        {
	            int part_number = atoi(message_buffer);

		        unsigned char file_buffer[FILE_PART_BUFFER];
		        int bytes_to_read = part_number * FILE_PART_BUFFER + FILE_PART_BUFFER > tcp_listener_thread_struct->file->size 
		            ? tcp_listener_thread_struct->file->size - part_number * FILE_PART_BUFFER : FILE_PART_BUFFER;
		        int bytes_read = pread(file_descriptor, file_buffer, bytes_to_read, part_number * FILE_PART_BUFFER);
		        send(new_socket, file_buffer, bytes_to_read, 0);
	        }
            else
	        {
	            uploading_finished = 1;
	        }
        }

        free(message_buffer);

        sprintf(buffer, "Finished uploading on port %d", tcp_port_description->port);
        ui_print(global_structure->windows_state, buffer, 1);
    }
    else
    {
        ui_print(global_structure->windows_state, "Failed to open uploading file", 1);
    }

    close(file_descriptor);
    free(file_path_buffer);

    close(new_socket);
    free(tcp_listener_thread_struct->file_description);
    close(tcp_port_description->socket_fd);
    free(tcp_port_description);
    free(tcp_listener_thread_struct);
}


void create_tcp_client(struct tcp_client_thread_struct *tcp_client_thread_struct)
{
	struct global_structure *global_structure = tcp_client_thread_struct->global_structure;

    struct node_description *node_description = tcp_client_thread_struct->node_description;

    int socket_fd;

    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        ui_print(global_structure->windows_state, "Failed to create (client TCP socket)", 1);
        free(node_description);
        free(tcp_client_thread_struct);
        return;
    }

    struct sockaddr_in listener_address;
    memset(&listener_address, 0, sizeof(listener_address));

    listener_address.sin_family = AF_INET;
    listener_address.sin_addr.s_addr = node_description->address;
    listener_address.sin_port = htons(node_description->port);

    if (connect(socket_fd, (struct sockaddr *)&listener_address, sizeof(listener_address)) < 0)
    {
        ui_print(global_structure->windows_state, "Could not connect (client TCP socket)", 1);
        close(socket_fd);
        free(node_description);
        free(tcp_client_thread_struct);
        return;
    }

    ui_print(global_structure->windows_state, "Start TCP client", 1);

    char *file_path_buffer = calloc(1, MAX_PATH_LENGTH);
    strcat(file_path_buffer, global_structure->root_path);
    strcat(file_path_buffer, "/");
    strcat(file_path_buffer, tcp_client_thread_struct->file->relative_path);

    int file_descriptor = open(file_path_buffer, O_WRONLY | O_CREAT, 0777);

    int current_offset = tcp_client_thread_struct->file_offset;
    int file_length = tcp_client_thread_struct->file_length;

    int previous_percents = -1;

    if (file_descriptor >= 0)
    {
        global_structure->files_list = g_list_append(global_structure->files_list, tcp_client_thread_struct->file);

        char *message_buffer = calloc(1, MESSAGE_BUFFER);

        char continue_receive = 1;

        while (continue_receive)
        {
        	int current_file_length = get_file_size(file_path_buffer);
            int current_percents = (int) ((((float)current_file_length) / tcp_client_thread_struct->total_file_length) * 100);

            if (current_percents != previous_percents)
            {
			    struct downloading_file *downloading_file = calloc(1, sizeof(struct downloading_file));
			    downloading_file->file_description = tcp_client_thread_struct->file_description;
			    downloading_file->message = calloc(1, MAX_LOG_ROW_LENGTH);
			    sprintf(downloading_file->message, " %-32s %dM/%dM     %d/100", tcp_client_thread_struct->file->name, 
			    	(current_file_length) / (1024*1024), 
			    	(tcp_client_thread_struct->total_file_length) / (1024*1024), current_percents);

                update_downloading_file(global_structure->windows_state, downloading_file);

                send(socket_fd, downloading_file->message, MAX_LOG_ROW_LENGTH, 0);

                previous_percents = current_percents;
            }

            if (current_offset < file_length)
            {
		        sprintf(message_buffer, "%d", (int) current_offset / FILE_PART_BUFFER);
		        send(socket_fd, message_buffer, MESSAGE_BUFFER, 0);

		        unsigned char file_buffer[FILE_PART_BUFFER];
		        int bytes_read = read(socket_fd , file_buffer, FILE_PART_BUFFER);
		        pwrite(file_descriptor, file_buffer, bytes_read, current_offset);

	            current_offset += FILE_PART_BUFFER;
            }
            else
            {
            	continue_receive = 0;
            }
        }

        free(message_buffer);

        char *close_message_buffer = "close";
        send(socket_fd, close_message_buffer, sizeof(close_message_buffer), 0);
    }
    else
    {
        ui_print(global_structure->windows_state, "Failed to open downloading file", 1);
    }

    close(file_descriptor);

    free(file_path_buffer);

    close(socket_fd);
    free(tcp_client_thread_struct->file_description);
    free(node_description);
    free(tcp_client_thread_struct);
}
