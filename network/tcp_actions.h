#include "../global_structures.h"
#include <netinet/in.h>


struct node_description
{
    int port;
    int address;
};

struct tcp_port_description
{
    int port;
    int socket_fd;
    struct sockaddr_in address;
};

struct tcp_listener_thread_struct
{
    struct global_structure *global_structure;
    struct tcp_port_description *tcp_port_description;
    struct file *file;
    char *file_description;
};

struct tcp_client_thread_struct
{
    struct global_structure *global_structure;
    struct node_description *node_description;
    struct file *file;
    char *file_description;
    int file_offset;
    int file_length;
    int total_file_length;
};

void take_tcp_port(struct global_structure *global_structure, struct tcp_port_description *tcp_port_description);

void create_tcp_listener(struct tcp_listener_thread_struct *tcp_listener_thread_struct);

void create_tcp_client(struct tcp_client_thread_struct *tcp_client_thread_struct);
