#include "udp_actions.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include "../ui/ui.h"
#include "../utils/consts.c"
#include "../command_line/command_interpreter.h"
#include "tcp_actions.h"
#include "../files/files_manager.h"


struct node_descriptions_wrapper
{
    GList *node_descriptions;
};


void create_udp_listener(struct global_structure *global_structure)
{
    int socket_fd;

    if ((socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        ui_print(global_structure->windows_state, "Failed to create socket", 1);
        sleep(ERROR_MESSAGE_DELAY);
        global_structure->close_program = 1;
        return;
    }

    int broadcast = 1;

    setsockopt(socket_fd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast));

    struct sockaddr_in listener_address, external_address;
    memset(&listener_address, 0, sizeof(listener_address));
    memset(&external_address, 0, sizeof(external_address));

    int port = DEFAULT_UDP_PORT;

    listener_address.sin_family = AF_INET;
    listener_address.sin_addr.s_addr = INADDR_ANY;
    listener_address.sin_port = htons(port);

    while (bind(socket_fd, (const struct sockaddr *) &listener_address, sizeof(listener_address)) < 0)
    {
        listener_address.sin_port = htons(++port);
    }

    char *buffer = calloc(1, MAX_LOG_ROW_LENGTH);
    sprintf(buffer, "UDP listener started on port %d", port);
    ui_print(global_structure->windows_state, buffer, 1);
    free(buffer);

    char *message_buffer = calloc(1, MESSAGE_BUFFER);
    int external_address_length = sizeof(external_address);

    while (!global_structure->close_program)
    {
        memset(message_buffer, 0, MESSAGE_BUFFER);
        int message_length = recvfrom(socket_fd, message_buffer, MESSAGE_BUFFER, 
            MSG_WAITALL, ( struct sockaddr *)&external_address, &external_address_length);

        struct file *file = has_file_description(global_structure, message_buffer);

        if (file != NULL)
        {
            struct node_description *node_description = calloc(1, sizeof(struct node_description));

            struct tcp_port_description *tcp_port_description = calloc(1, sizeof(struct tcp_port_description));
            take_tcp_port(global_structure, tcp_port_description);

            node_description->port = tcp_port_description->port;

            pthread_t *tcp_listener_thread = NULL;

            if (tcp_port_description->port >= 0)
            {
                struct tcp_listener_thread_struct *tcp_listener_thread_struct = calloc(1, sizeof(struct tcp_listener_thread_struct));
                tcp_listener_thread_struct->global_structure = global_structure;
                tcp_listener_thread_struct->tcp_port_description = tcp_port_description;
                tcp_listener_thread_struct->file = file;
                char *file_description_buffer = calloc(1, strlen(message_buffer));
                strcpy(file_description_buffer, message_buffer);
                tcp_listener_thread_struct->file_description = file_description_buffer;

                tcp_listener_thread = malloc(sizeof(pthread_t));
                pthread_create(tcp_listener_thread, NULL, (void *) create_tcp_listener, tcp_listener_thread_struct);
            }

            sendto(socket_fd, node_description, sizeof(node_description), 
                MSG_CONFIRM, (const struct sockaddr *) &external_address, external_address_length);

            free(node_description);

            if (tcp_listener_thread != NULL)
            {
                pthread_join(*tcp_listener_thread, NULL);
                free(tcp_listener_thread);
            }
        }
    }

    free(message_buffer);
    close(socket_fd);
}


void find_nodes(struct global_structure *global_structure, char *file_description, struct node_descriptions_wrapper *nodes_wrapper, int port)
{
    int socket_fd;

    if ((socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        ui_print(global_structure->windows_state, "Could not find suitable nodes (socket creation error)", 1);
        sleep(ERROR_MESSAGE_DELAY);
        return;
    }

    int broadcast = 1;

    struct timeval timeval;
    timeval.tv_sec = 1;
    timeval.tv_usec = 0;

    setsockopt(socket_fd, SOL_SOCKET, SO_RCVTIMEO, &timeval, sizeof(timeval));

    struct sockaddr_in listener_address, respondent_address;
    memset(&listener_address, 0, sizeof(listener_address));
    memset(&respondent_address, 0, sizeof(respondent_address));

    listener_address.sin_family = AF_INET;
    listener_address.sin_addr.s_addr = INADDR_ANY;
    listener_address.sin_port = htons(port);

    sendto(socket_fd, file_description, strlen(file_description), MSG_CONFIRM, 
        (const struct sockaddr *) &listener_address, sizeof(listener_address)); 

    int listener_address_length = sizeof(listener_address);
    int respondent_address_length = sizeof(respondent_address);

    char continue_search = 1;

    while (continue_search)
    {
        char *message_buffer = calloc(1, MESSAGE_BUFFER);

        memset(message_buffer, 0, MESSAGE_BUFFER);

        int message_length = recvfrom(socket_fd, (char *)message_buffer, MESSAGE_BUFFER, 
            MSG_WAITALL, (struct sockaddr *) &respondent_address, &respondent_address_length);

        struct node_description *node_description = NULL;

        if (message_length >= 0)
        {
            node_description = (struct node_description *) message_buffer;

            if (node_description->port >= 0)
            {
                node_description->address = respondent_address.sin_addr.s_addr;

                nodes_wrapper->node_descriptions = g_list_append(nodes_wrapper->node_descriptions, node_description);
            }
        }
        else
        {
            continue_search = 0;
        }
    }

    close(socket_fd);
}


void find_and_download(struct global_structure *global_structure, char *file_description)
{
    struct node_descriptions_wrapper *nodes_wrapper = calloc(1, sizeof(struct node_descriptions_wrapper));
    find_nodes(global_structure, file_description, nodes_wrapper, DEFAULT_UDP_PORT);
    find_nodes(global_structure, file_description, nodes_wrapper, DEFAULT_UDP_PORT + 1);

    int suitable_nodes_count = g_list_length(nodes_wrapper->node_descriptions);

    GList *current_element = g_list_first(nodes_wrapper->node_descriptions);

    int i = 0;

    if (suitable_nodes_count == 0)
    {
        ui_print(global_structure->windows_state, "Specified file wasn't found", 1);
        return;
    }

    GList *threads = NULL;

    while (current_element != NULL)
    {
        struct node_description *node_description = (struct node_description *) current_element->data;

        struct tcp_client_thread_struct *tcp_client_thread_struct = calloc(1, sizeof(struct tcp_client_thread_struct));
        tcp_client_thread_struct->global_structure = global_structure;
        tcp_client_thread_struct->node_description = node_description;
        tcp_client_thread_struct->file = description_to_file(file_description);
        char *file_description_buffer = calloc(1, strlen(file_description));
        strcpy(file_description_buffer, file_description);
        tcp_client_thread_struct->file_description = file_description_buffer;
        tcp_client_thread_struct->file_offset = tcp_client_thread_struct->file->size / FILE_PART_BUFFER / suitable_nodes_count * FILE_PART_BUFFER * i;

        if (i < suitable_nodes_count - 1)
        {
            tcp_client_thread_struct->file_length = tcp_client_thread_struct->file->size / FILE_PART_BUFFER / suitable_nodes_count * FILE_PART_BUFFER * (i + 1);
        }
        else
        {
            tcp_client_thread_struct->file_length = tcp_client_thread_struct->file->size;
        }

        tcp_client_thread_struct->total_file_length = tcp_client_thread_struct->file->size;

        pthread_t *tcp_client_thread = malloc(sizeof(pthread_t));
        pthread_create(tcp_client_thread, NULL, (void *) create_tcp_client, tcp_client_thread_struct);

        threads = g_list_append(threads, tcp_client_thread);

        current_element = current_element->next;
        i++;
    }

    current_element = g_list_first(threads);

    while (current_element != NULL)
    {
        pthread_t *tcp_client_thread = (pthread_t *) current_element->data;
        pthread_join(*tcp_client_thread, NULL);
        free(tcp_client_thread);
        current_element = current_element->next;
    }

    char *buffer = calloc(1, MAX_LOG_ROW_LENGTH);
    sprintf(buffer, "Downloaded file with description: %s", file_description);
    ui_print(global_structure->windows_state, buffer, 1);
    free(buffer);

    g_list_free(threads);
    g_list_free(nodes_wrapper->node_descriptions);
    free(nodes_wrapper);
}
