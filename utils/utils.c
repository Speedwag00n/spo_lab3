#include <ctype.h>
#include <string.h>


void parse_args(const char *command, char *args[], int args_number)
{
    int arg_number = 0;
    int args_parsed = 0;

    while (!args_parsed)
    {
        if (arg_number >= args_number || *command == '\0') {
            args_parsed = 1;
        }
        else if (isspace(*command))
        {
            command++;
        }
        else if (*command == '"' || *command == '\'')
        {
            int quote_char = *command;
            const char* first_char = ++command;

            while (*command != quote_char && *command != '\0')
            {
                command++;
            }

            strncpy(args[arg_number++], first_char, command++ - first_char);

            if (*command == '\0')
            {
                args_parsed = 1;
            }
            else
            {
                command++;
            }
        }
        else if (*command > 32)
        {
            const char* first_char = command;

            while (*command > 32)
            {
                command++;
            }

            strncpy(args[arg_number++], first_char, command - first_char);

            if (*command == '\0')
            {
                args_parsed = 1;
            }
            else
            {
                command++;
            }
        }
        else
        {
            args_parsed = 1;
        }
    }
}


void normalize_path(char *normalized_path)
{
    int path_normalized = 0;
    int i = 0;

    while (!path_normalized)
    {
        if (i + 1 < strlen(normalized_path) && ( (i == 0 && normalized_path[i] == '.' && normalized_path[i + 1] == '/')
            || (i != 0 && normalized_path[i - 1] == '/' && normalized_path[i] == '.' && normalized_path[i + 1] == '/') ))
        {
            if (i < strlen(normalized_path) - 2)
            {
                int u = i;
                for (; u < strlen(normalized_path) - 2; u++)
                {
                    normalized_path[u] = normalized_path[u + 2];
                }
                normalized_path[u] = 0;
                normalized_path[u + 1] = 0;
            }
            else
            {
                normalized_path[i] = 0;
                normalized_path[i + 1] = 0;
            }
        }
        else if ((i == 0 || normalized_path[i - 1] == '/') && i + 1 == strlen(normalized_path) && normalized_path[i] == '.')
        {
            if (i != 0)
            {
                normalized_path[i - 1] = 0;  
            }
            normalized_path[i] = 0;              
        }
        else
        {
            if (i >= strlen(normalized_path))
            {
                path_normalized = 1;
            }
            i++;
        }
    }
}
